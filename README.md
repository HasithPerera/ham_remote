# Ham Remote

This project contains the workarounds for echolink

## Android app setup

1. [MQTT Dash](https://play.google.com/store/apps/details?id=net.routix.mqttdash&gl=US&pli=1) is used as the mobile client to control the station. Install the app on your mobile device
2. A public server is used to send and recieve MQTT messaged. currently [mqtt.eclipseproject.io](https://mqtt.eclipseprojects.io/)
3. App setup instructions
  - click on the add icon. top left conor in the apps main screen
  - Name field: Echolink W8CUL
  - Address: mqtt.eclipseproject.io
  - port: 1883
  - save and return to the home screen
  - You will see a new line in the home screen with the Name. select that
  - During the first use to sync the controlls and commands do the following
    - click on the two arrow icon
    - under the new box enter `KE8TJE/app`
    - click "SUBSCRIBE AND WAIT FOR METRIC" only. Note other settings here will remove the app configuration.
    - if all the configuration is correct a 4 window screen should pop up.
    - follow this process anytime you want to get the latest update

# To DO

- [x] Mqtt cmd structure for quick ip check
- [x] added SVXLINK start stop commands
- [ ] public ip update from echolink list

