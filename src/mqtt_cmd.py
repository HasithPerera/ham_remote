#!/usr/bin/python3
import sys
print(sys.version)


import paho.mqtt.client as mqtt
import time
import subprocess
import config as cfg
#from urllib2 import urlopen
import requests
import threading
import socket
import re



def findmy_ip():
    return requests.get('https://ip.42.pl/raw').text

svx_cmds = ['start_svx','stop_svx','restart_svx']
#def run_rtltcp():
#    ahe_exec = subprocess.call(["rtl_tcp","-a","192.168.1.250"],stdin=None,stdout=None,stderr=None)

def get_ip():
	ahe_exec = subprocess.check_output('ifconfig',shell=False)
	ips = re.findall('inet [0-9]+.[0-9]+.[0-9]+.[0-9]+',ahe_exec.decode('utf-8'))
	str1 = ''
	for m in ips:
		str1 = str1+ m[5:] + "\n"
	return str1


#def killall(client):
#    ahe_exec = subprocess.call(["killall","rtl_tcp"],stdin=None,stdout=None,stderr=None)

#    client.publish("/ahe/home-sdr/tcp/status",'killtcp')

def echolink_ctl(client,state):

    if (state=='stop_svx'):
        
        try:
            ahe_exec = subprocess.call(["systemctl","stop",'svxlink'],stdin=None,stdout=None,stderr=None)
            ahe_exec = subprocess.call(["picocom","/dev/ttyUSB0","&"],stdin=None,stdout=None,stderr=None)
            client.publish(cfg.root_mqtt+"echolink/state",'stop_svx')
        except:
            client.publish(cfg.root_mqtt+"echolink/state",'ERR!')

    
    if (state=='start_svx'):
        try:
            ahe_exec = subprocess.call(["systemctl","start",'svxlink'],stdin=None,stdout=None,stderr=None)
            ahe_exec = subprocess.call(["killall","picocom"],stdin=None,stdout=None,stderr=None)
            client.publish(cfg.root_mqtt+"echolink/state",'start_svx')
        except:
            client.publish(cfg.root_mqtt+"echolink/state",'ERR!')

def on_connect(client, userdata, flags, rc):
    client.subscribe(cfg.root_mqtt+"cmd")

def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))
    if 'cmd'in msg.topic:
        if msg.payload.decode() == 'ip':
            client.publish(cfg.root_mqtt+"echolink/ip",get_ip())
        
        if msg.payload.decode() == 'net':
            pass
            #t = threading.Thread(target=run_rtltcp,name='rtltcp mqtt')
            #t.daemon = True
            #t.start()
   #     if msg.payload.decode() == 'tcp':
	#		pass
            #t = threading.Thread(target=run_rtltcp,name='rtltcp mqtt')
            #t.daemon = True
            #t.start()
            #client.publish("/ahe/home-sdr/tcp/status",'tcp')


        if (msg.payload.decode() in svx_cmds):
            echolink_ctl(client,msg.payload.decode())
            #print('got svx_cmd')
            

if __name__=="__main__":
#	get_ip()
		
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message

    client.connect(cfg.mqtt, 1883, 60)

    client.loop_forever()
    

